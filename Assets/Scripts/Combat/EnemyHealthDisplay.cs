﻿using System;
using System.Collections;
using System.Collections.Generic;
using RPG.Resources;
using UnityEngine;
using UnityEngine.UI;

namespace RPG.Combat
{
    public class EnemyHealthDisplay : MonoBehaviour
    {
        Fighter fighter;

        private void Awake() 
        {
            fighter = GameObject.FindWithTag("Player").GetComponent<Fighter>();
        }

        private void Update() 
        {
            if (fighter.GetTarget() == null)
            {
                GetComponent<Text>().text = "No Target";
                return;
            }
            Health health = fighter.GetTarget(); 
            //using format so it wont display the decimal
            GetComponent<Text>().text = String.Format("{0:0}%",health.GetPercentage());
        }
    }
}
