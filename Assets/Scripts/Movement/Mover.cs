using RPG.Core;
using RPG.Resources;
using RPG.Saving;
using UnityEngine;
using UnityEngine.AI;

namespace RPG.Movement
{
    public class Mover : MonoBehaviour, IAction, ISaveable
{
    [SerializeField] Transform target;
    [SerializeField] float maxSpeed = 6f;

    NavMeshAgent navMeshAgent;
    Health health;

    private void Start() 
    {
        // to minimalis calling the component
        navMeshAgent = GetComponent<NavMeshAgent>();
        health = GetComponent<Health>();
    }

    // Ray lastRay;
    // Update is called once per frame
    void Update()
    {
        //if dead then disable the navmesh agent
        navMeshAgent.enabled = !health.IsDead();
        //getting mouse direction 
        //if(Input.GetMouseButtonDown(0)) note that this only getting it every once


        //updating animation
        UpdateAnimator();
    }


    public void StartMoveAction(Vector3 destination, float speedFraction)
    {
        //start action scheduler
        GetComponent<ActionScheduler>().StartAction(this);
        //move the navmesh
        MoveTo(destination, speedFraction);
    }
    public void MoveTo(Vector3 destination, float speedFraction)
    {
        navMeshAgent.destination = destination;
        navMeshAgent.speed = maxSpeed * Mathf.Clamp01(speedFraction);
        navMeshAgent.isStopped = false;
    }

    public void Cancel()
    {
        //stop the moving navmesh
        navMeshAgent.isStopped = true;
    }


    public void UpdateAnimator()
    {
        //getting the global velocity
        Vector3 Velocity = navMeshAgent.velocity;
        //convert the velocity to local
        Vector3 localVelocity = transform.InverseTransformDirection(Velocity);
        float speed = localVelocity.z;

        GetComponent<Animator>().SetFloat("ForwardSpeed", speed);
    }



        public object CaptureState()
        {
            return new SerializableVector3(transform.position);
        }

        public void RestoreState(object state)
        {
            SerializableVector3 position = (SerializableVector3) state;

            GetComponent<NavMeshAgent>().enabled = false;
            transform.position = position.ToVector();
            GetComponent<NavMeshAgent>().enabled = true;
        }

        //debug to show ray direction
        // Debug.DrawRay(lastRay.origin, lastRay.direction * 100);

        // lastRay = Camera.main.ScreenPointToRay(Input.mousePosition);
        // Geting the target destination
        //  GetComponent<NavMeshAgent>().destination = target.position;
    }


}